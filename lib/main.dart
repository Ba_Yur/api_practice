import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

import 'models/person.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Person currentPerson;

  void _addPerson() async {
    final data = await http.get(
        'https://randomapi.com/api/6de6abfedb24f889e0b5f675edc50deb?fmt=raw&sole');
    print(json.decode(data.body));

    setState(() {
      currentPerson = getPersonData(json.decode(data.body));
    });
  }

  Person getPersonData(List<dynamic> list) {
    Random random = Random();
    Map<String, dynamic> personData = list[random.nextInt(list.length)];
    print(personData);
    return Person(
        firstName: personData['first'],
        lastName: personData['last'],
        email: personData['email'],
        address: personData['address'],
        balance: personData['balance']);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('API practice'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
              'Person first name: ${currentPerson != null ? currentPerson.firstName : ''}'),
          Text(
              'Person last name: ${currentPerson != null ? currentPerson.lastName : ''}'),
          Text(
              'Person email: ${currentPerson != null ? currentPerson.email : ''}'),
          Text(
              'Person address: ${currentPerson != null ? currentPerson.address : ''}'),
          Text(
              'Person balance: ${currentPerson != null ? currentPerson.balance : ''}'),
          Center(
            child: ElevatedButton(
              child: Text('Tap me'),
              onPressed: _addPerson,
            ),
          ),
        ],
      ),
    );
  }
}
