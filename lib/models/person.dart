import 'package:flutter/cupertino.dart';

class Person {
   String firstName;
   String lastName;
   String email;
   String address;
   String balance;

  Person({
    @required this.firstName,
    @required this.lastName,
    @required this.email,
    @required this.address,
    @required this.balance,
  });
}
